Site for Pudlicote Farm
Spectral by HTML5 UP
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)


- It uses flexbox, which eliminates all kinds of terrible hacks and clunky layout
  stopgaps (like CSS grid systems).


	Icons:
		Font Awesome (fontawesome.io)

	Other:
		jQuery (jquery.com)
		Scrollex (github.com/ajlkn/jquery.scrollex)
		Responsive Tools (github.com/ajlkn/responsive-tools)
